import express from "express";
import {mainRoute, withParams, withId, withBody, withCheck} from './controller/basicController.js'
import { basicMiddleware } from "./middleware/basicMiddleware.js";
import { bio } from "./controller/biodataController.js";
import { checkAuthKey } from "./middleware/checkKey.js";
// import { get as getProduct, create as createProduct, 
//     update as updateProduct, destroy as deleteProduct, 
//     find as findProduct } from "./controller/productController.js"
import productApi from './api/productApi.js';
import bookApi from './api/bookApi.js';
import officeApi from './api/officeApi.js';

const router = express.Router()

router.get('/', basicMiddleware, mainRoute)
router.get('/check-health', withCheck) //task
router.get('/biodata', checkAuthKey,bio) //task
router.use('/product', productApi)
router.use('/book', bookApi)
router.use('/office', officeApi)
router.get('/with-param', withParams)
router.get('/:id', withId)
router.post('/body', withBody)

// router.get('/product/list', getProduct)
// router.get('/product/create', createProduct)
// router.get('/product/update', updateProduct)
// router.get('/product/delete', deleteProduct)
// router.get('/product/find', findProduct)

export default router 