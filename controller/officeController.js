import Office from '../resources/office.js'

export const create = (req, res) => {
    const office = Office.create(req.body)

    return res.status(200).json(office)
}

export const list = (req, res) => {
    const offices = Office.list();

    return res.status(200).json(offices)
}

export const find = (req, res) => {
    const office = Office.find(req.params.id);

    return res.status(200).json(office)
}