export const withCheck = (req, res) => {
    return res.send('Application is running')
} //task

export const mainRoute = (req, res) => {
    return res.send('hello world')
}

export const withParams = (req, res) => {
    return res.send(`hello the query param is ${req.query.nama}`)
}

export const withId = (req, res) => {
    return res.send(`hello the param is ${req.params.id}`)
}

export const withBody = (req, res) => {
    return res.send(`hello the body is ${req.body.nama}`)
}